package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		System.out.println(XMLTools.validateFile(xmlFileName));

		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		var DOMFlowers = domController.parseFlowers();
		// PLACE YOUR CODE HERE
		DOMController.sort(DOMFlowers);
		// sort (case 1)
		// PLACE YOUR CODE HERE
		
		// save
		String outputXmlFile = "output.dom.xml";
		XMLTools.writeFlowersXML(DOMFlowers, outputXmlFile);
		// PLACE YOUR CODE HERE

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		var SAXFlowers = saxController.parseFlowers();

		// PLACE YOUR CODE HERE
		SAXController.sort(SAXFlowers);
		// sort  (case 2)
		// PLACE YOUR CODE HERE

		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		XMLTools.writeFlowersXML(SAXFlowers, outputXmlFile);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		var STAXFlowers = staxController.parseFlowers();

		STAXController.sort(STAXFlowers);
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		XMLTools.writeFlowersXML(STAXFlowers, outputXmlFile);
	}

}
