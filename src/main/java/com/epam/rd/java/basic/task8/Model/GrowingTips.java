package com.epam.rd.java.basic.task8.Model;

public class GrowingTips {
    public static final String C_NAME = "growingTips";
    public static final String F_TEMPERATURE = "tempreture";
    public static final String A_LIGHT_REQUIRING = "lightRequiring";
    public static final String F_LIGHTING = "lighting";
    public static final String F_WATERING = "watering";
    public static final String A_MEASURE = "measure";
    public static final String YES = "yes";
    public static final String NO = "no";

    private Integer temperature;
    private String temperatureMeasure;
    private boolean lightRequiring;
    private Integer watering;
    private String wateringMeasure;

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public boolean isLightRequiring() {
        return lightRequiring;
    }

    public String isLightRequiringString() {
        return lightRequiring ? YES: NO;
    }

    public void setLightRequiring(boolean lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = YES.equals(lightRequiring);
    }

    public Integer getWatering() {
        return watering;
    }

    public void setWatering(Integer watering) {
        this.watering = watering;
    }

    public void setWatering(String watering) {
        try {
            this.watering = Integer.valueOf(watering);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public void setTemperature(String temperature) {
        try {
            this.temperature = Integer.valueOf(temperature);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public String getTemperatureMeasure() {
        return temperatureMeasure;
    }

    public void setTemperatureMeasure(String temperatureMeasure) {
        this.temperatureMeasure = temperatureMeasure;
    }

    public String getWateringMeasure() {
        return wateringMeasure;
    }

    public void setWateringMeasure(String wateringMeasure) {
        this.wateringMeasure = wateringMeasure;
    }
}
