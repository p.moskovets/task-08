package com.epam.rd.java.basic.task8.Model;

public enum Multiplying {

    Leaves("листья"),
    Stalks("черенки"),
    Seeds("семена");

    public static final String C_NAME = "multiplying";

    private final String value;

    Multiplying(String s) {
        value = s;
    }

    public static Multiplying getValue(String s) {
        for (var val: values()) {
            if (s.equals(val.value)) {
                return val;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }
}

