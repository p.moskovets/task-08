package com.epam.rd.java.basic.task8.Model;

public enum Soil {
    Podzolic("подзолистая"),
    Ground("грунтовая"),
    SodPodzolic("дерново-подзолистая");

    public static final String C_NAME = "soil";

    private final String value;

    Soil(String s) {
        value = s;
    }

    public static Soil getValue(String s) {
        for (var val: values()) {
            if (s.equals(val.value)) {
                return val;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }

}
