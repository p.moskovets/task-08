package com.epam.rd.java.basic.task8.Model;

public class VisualParameters {

    public static final String C_NAME = "visualParameters";
    public static final String F_STEM_COLOUR = "stemColour";
    public static final String F_LEAF_COLOUR = "leafColour";
    public static final String F_AVE_LEN_FLOWER = "aveLenFlower";
    public static final String A_MEASURE = "measure";

    private String stemColour;
    private String leafColour;
    private Integer aveLenFlower;
    private String measure;

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }



    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public Integer getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(Integer aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public void setAveLenFlower(String aveLenFlower) {
        try {
            this.aveLenFlower = Integer.parseInt(aveLenFlower);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
}
