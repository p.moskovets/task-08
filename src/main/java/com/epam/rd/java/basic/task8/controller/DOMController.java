package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for DOM parser.
 *
 * Reads the file passed to the constructor and converts it to the list of Flower objects
 *
 * @author  Pavlo Moskovets
 *
 */
public class DOMController {

	private String xmlFileName;
	private final List<Flower> flowers = new ArrayList<>();

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	private void parseVisualParameters(VisualParameters visualParameters, Node node) {
		NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			if (nodeList.item(i).getLocalName() != null)
				switch (nodeList.item(i).getLocalName()) {
					case VisualParameters.F_STEM_COLOUR:
						visualParameters.setStemColour(nodeList.item(i).getTextContent());
						break;
					case VisualParameters.F_LEAF_COLOUR:
						visualParameters.setLeafColour(nodeList.item(i).getTextContent());
						break;
					case VisualParameters.F_AVE_LEN_FLOWER:
						visualParameters.setMeasure(nodeList
								.item(i)
								.getAttributes()
								.getNamedItem(VisualParameters.A_MEASURE)
								.getTextContent());
						visualParameters.setAveLenFlower(nodeList.item(i).getTextContent());
						break;
				}
		}
	}

	private void parseGrowingTips(GrowingTips growingTips, Node node) {
		NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			if (nodeList.item(i).getLocalName() != null)
				switch (nodeList.item(i).getLocalName()) {
					case GrowingTips.F_TEMPERATURE:
						growingTips.setTemperatureMeasure(nodeList
								.item(i)
								.getAttributes()
								.getNamedItem(GrowingTips.A_MEASURE)
								.getTextContent());
						growingTips.setTemperature(nodeList.item(i).getTextContent());
						break;
					case GrowingTips.F_LIGHTING:
						growingTips.setLightRequiring(nodeList
								.item(i)
								.getAttributes()
								.getNamedItem(GrowingTips.A_LIGHT_REQUIRING)
								.getTextContent());
						break;
					case GrowingTips.F_WATERING:
						growingTips.setWateringMeasure(nodeList
								.item(i)
								.getAttributes()
								.getNamedItem(GrowingTips.A_MEASURE)
								.getTextContent());
						growingTips.setWatering(nodeList.item(i).getTextContent());
						break;
				}
		}
	}

	private void parseFlower(Node node) {
		Flower flower = new Flower();
		NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			if (nodeList.item(i).getLocalName() != null)
				switch (nodeList.item(i).getLocalName()) {
					case Flower.F_NAME:
						flower.setName(nodeList.item(i).getTextContent());
						break;
					case Flower.F_ORIGIN:
						flower.setOrigin(nodeList.item(i).getTextContent());
						break;
					case Soil.C_NAME:
						flower.setSoil(nodeList.item(i).getTextContent());
						break;
					case Multiplying.C_NAME:
						flower.setMultiplying(nodeList.item(i).getTextContent());
						break;
					case VisualParameters.C_NAME:
						VisualParameters visualParameters = new VisualParameters();
						parseVisualParameters(visualParameters, nodeList.item(i));
						flower.setVisualParameters(visualParameters);
						break;
					case GrowingTips.C_NAME:
						GrowingTips growingTips = new GrowingTips();
						parseGrowingTips(growingTips, nodeList.item(i));
						flower.setGrowingTips(growingTips);
						break;
				}
		}
		flowers.add(flower);
	}

	public List<Flower> parseFlowers() {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		documentBuilderFactory.setNamespaceAware(true);
		try {
			documentBuilderFactory.setFeature(SAXController.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
			documentBuilderFactory.setFeature(SAXController.FEATURE_TURN_VALIDATION_ON, true);
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.parse(xmlFileName);
			Element root = document.getDocumentElement();
			NodeList nodeList = root.getChildNodes();
			for (int i = 0; i < nodeList.getLength(); i++) {
				if (Flower.C_NAME.equals(nodeList.item(i).getLocalName())) {
					parseFlower(nodeList.item(i));
				}
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}


		return flowers;
	}


	/**
	 * @param flowers
	 *
	 */
	public static void sort(List<Flower> flowers) {
		flowers.sort(Comparator.comparing(Flower::getName));
	}

}
