package com.epam.rd.java.basic.task8.controller.SAX;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.function.Consumer;

public abstract class CustomHandler extends DefaultHandler {

    protected static void setAttribute(Attributes attributes, String parameterName , Consumer<String> dest) {
        for (int i = 0; i < attributes.getLength(); i++) {
            if (attributes.getLocalName(i).equals(parameterName)) {
                dest.accept(attributes.getValue(i));
                break;
            }
        }
    }
}
