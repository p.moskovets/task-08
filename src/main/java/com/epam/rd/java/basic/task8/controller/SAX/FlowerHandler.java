package com.epam.rd.java.basic.task8.controller.SAX;

import com.epam.rd.java.basic.task8.Model.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.function.Consumer;

public class FlowerHandler extends CustomHandler {

    public Flower getFlower() {
        return flower;
    }

    private final Flower flower = new Flower();


    private Consumer<String> consumer;
    private CustomHandler handler;
    private boolean isPrimitive;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (localName) {
            case Flower.F_NAME:
                consumer = flower::setName;
                isPrimitive = true;
                break;
            case Flower.F_ORIGIN:
                consumer = flower::setOrigin;
                isPrimitive = true;
                break;
            case GrowingTips.C_NAME:
                GrowingTips growingTips = new GrowingTips();
                flower.setGrowingTips(growingTips);
                handler = new GrowingTipsHandler(growingTips);
                isPrimitive = false;
                break;
            case VisualParameters.C_NAME:
                VisualParameters visualParameters = new VisualParameters();
                flower.setVisualParameters(visualParameters);
                handler = new VisualParametersHandler(visualParameters);
                isPrimitive = false;
                break;
            case Multiplying.C_NAME:
                consumer = flower::setMultiplying;
                isPrimitive = true;
                break;
            case Soil.C_NAME:
                consumer = flower::setSoil;
                isPrimitive = true;
                break;

        }
        if (!isPrimitive && handler != null)
            handler.startElement(uri, localName, qName, attributes);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (!isPrimitive && handler != null)
            handler.endElement(uri, localName, qName);
        else {
            consumer = null;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (isPrimitive) {
            if (consumer != null)
                consumer.accept(String.valueOf(ch, start, length));
        } else {
            if(handler != null)
                handler.characters(ch, start, length);
        }
    }

}
