package com.epam.rd.java.basic.task8.controller.SAX;

import com.epam.rd.java.basic.task8.Model.GrowingTips;
import org.xml.sax.Attributes;

import org.xml.sax.SAXException;

import java.util.function.Consumer;

public class GrowingTipsHandler extends CustomHandler {

    private final GrowingTips growingTips;
    private Consumer<String> consumer;

    public GrowingTipsHandler(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (localName) {
            case GrowingTips.F_TEMPERATURE:
                consumer = growingTips::setTemperature;
                setAttribute(attributes, GrowingTips.A_MEASURE, growingTips::setTemperatureMeasure);
                break;
            case GrowingTips.F_LIGHTING:
                setAttribute(attributes, GrowingTips.A_LIGHT_REQUIRING, growingTips::setLightRequiring);
                break;
            case GrowingTips.F_WATERING:
                consumer = growingTips::setWatering;
                setAttribute(attributes, GrowingTips.A_MEASURE, growingTips::setWateringMeasure);
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        consumer = null;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (consumer != null)
            consumer.accept(String.valueOf(ch, start, length));
    }

}
