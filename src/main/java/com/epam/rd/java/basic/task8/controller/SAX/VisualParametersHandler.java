package com.epam.rd.java.basic.task8.controller.SAX;

import com.epam.rd.java.basic.task8.Model.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.function.Consumer;


public class VisualParametersHandler extends CustomHandler {

    private final VisualParameters visualParameters;
    private Consumer<String> consumer;

    public VisualParametersHandler(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (localName) {
            case VisualParameters.F_STEM_COLOUR:
                consumer = visualParameters::setStemColour;
                break;
            case VisualParameters.F_LEAF_COLOUR:
                consumer = visualParameters::setLeafColour;
                break;
            case VisualParameters.F_AVE_LEN_FLOWER:
                consumer = visualParameters::setAveLenFlower;
                setAttribute(attributes, VisualParameters.A_MEASURE, visualParameters::setMeasure);
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        consumer = null;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (consumer != null)
            consumer.accept(String.valueOf(ch, start, length));
    }

}
