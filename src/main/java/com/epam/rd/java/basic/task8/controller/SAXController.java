package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Model.Flower;
import com.epam.rd.java.basic.task8.controller.SAX.FlowerHandler;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for SAX parser.
 *
 * Reads the file passed to the constructor and converts it to the list of Flower objects
 *
 * @author  Pavlo Moskovets
 *
 */
public class SAXController extends DefaultHandler {

	public static final String FEATURE_TURN_VALIDATION_ON = "http://xml.org/sax/features/validation";
	public static final String FEATURE_TURN_SCHEMA_VALIDATION_ON =	"http://apache.org/xml/features/validation/schema";
	
	private final String xmlFileName;
	private final List<Flower> flowers = new ArrayList<>();
	private FlowerHandler handler;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (Flower.C_NAME.equals(localName)) {
			handler = new FlowerHandler();
		}
		if (handler != null)
			handler.startElement(uri, localName, qName, attributes);
	}


	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (Flower.C_NAME.equals(localName)) {
			flowers.add(handler.getFlower());
		}
		if (handler != null)
			handler.endElement(uri, localName, qName);
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (handler != null)
			handler.characters(ch, start, length);
	}

	public List<Flower> parseFlowers() {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);
		try {
			factory.setFeature(FEATURE_TURN_VALIDATION_ON, true);
			factory.setFeature(FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
		} catch (ParserConfigurationException | SAXNotRecognizedException | SAXNotSupportedException e) {
			e.printStackTrace();
		}
		SAXParser parser = null;
		try {
			parser = factory.newSAXParser();
			parser.parse(xmlFileName, this);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		return flowers;
	}
	public static void sort(List<Flower> flowers) {
		flowers.sort(Comparator.comparingInt(o -> o.getGrowingTips().getTemperature()));
	}

}