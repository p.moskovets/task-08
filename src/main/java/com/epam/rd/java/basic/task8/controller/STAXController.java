package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Model.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for StAX parser.
 *
 * Reads the file passed to the constructor and converts it to the list of Flower objects
 *
 * @author  Pavlo Moskovets
 *
 */
public class STAXController extends DefaultHandler {

	private final String UTF_8_ENCODING = "UTF-8";

	private final String xmlFileName;
	private final List<Flower> flowers = new ArrayList<>();

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	private boolean isEmptyContent(XMLEvent event) {
		return event.isCharacters() && event.asCharacters().isWhiteSpace();
	}

	private String getAttributeValue(StartElement startElement, String attributeName) {
		var attributes = startElement.getAttributes();
		while (attributes.hasNext()) {
			var attr = attributes.next();
			if (attributeName.equals(attr.getName().getLocalPart()))
				return attr.getValue();
		}
		return null;
	}

	private void parseGrowingTips(GrowingTips growingTips, XMLEventReader reader) throws XMLStreamException {
		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();
			if (isEmptyContent(event))
				continue;
			if (event.isEndElement()) {
				if (GrowingTips.C_NAME.equals(event.asEndElement().getName().getLocalPart()))
					return;
			}
			if (event.isStartElement()) {
				switch (event.asStartElement().getName().getLocalPart()) {
					case GrowingTips.F_TEMPERATURE:
						growingTips.setTemperatureMeasure(getAttributeValue(event.asStartElement(), GrowingTips.A_MEASURE));
						event = reader.nextEvent();
						growingTips.setTemperature(event.asCharacters().getData());
						break;
					case GrowingTips.F_LIGHTING:
						growingTips.setLightRequiring(getAttributeValue(event.asStartElement(), GrowingTips.A_LIGHT_REQUIRING));
						break;
					case GrowingTips.F_WATERING:
						growingTips.setWateringMeasure(getAttributeValue(event.asStartElement(), GrowingTips.A_MEASURE));
						event = reader.nextEvent();
						growingTips.setWatering(event.asCharacters().getData());
						break;
				}
			}
		}
	}

	private void parseVisualParameters(VisualParameters visualParameters, XMLEventReader reader) throws XMLStreamException {
		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();
			if (isEmptyContent(event))
				continue;
			if (event.isEndElement()) {
				if (VisualParameters.C_NAME.equals(event.asEndElement().getName().getLocalPart()))
					return;
			}
			if (event.isStartElement()) {
				switch (event.asStartElement().getName().getLocalPart()) {
					case VisualParameters.F_STEM_COLOUR:
						event = reader.nextEvent();
						visualParameters.setStemColour(event.asCharacters().getData());
						break;
					case VisualParameters.F_LEAF_COLOUR:
						event = reader.nextEvent();
						visualParameters.setLeafColour(event.asCharacters().getData());
						break;
					case VisualParameters.F_AVE_LEN_FLOWER:
						visualParameters.setMeasure(getAttributeValue(event.asStartElement(), VisualParameters.A_MEASURE));
						event = reader.nextEvent();
						visualParameters.setAveLenFlower(event.asCharacters().getData());
						break;
				}
			}
		}
	}

	private void parseFlower(Flower flower, XMLEventReader reader) throws XMLStreamException {
		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();
			if (isEmptyContent(event))
				continue;
			if (event.isEndElement()) {
				if (Flower.C_NAME.equals(event.asEndElement().getName().getLocalPart()))
					return;
			}
			if (event.isStartElement()) {
				switch (event.asStartElement().getName().getLocalPart()) {
					case Flower.F_NAME:
						event = reader.nextEvent();
						flower.setName(event.asCharacters().getData());
						break;
					case Flower.F_ORIGIN:
						event = reader.nextEvent();
						flower.setOrigin(event.asCharacters().getData());
						break;
					case Soil.C_NAME:
						event = reader.nextEvent();
						flower.setSoil(event.asCharacters().getData());
						break;
					case Multiplying.C_NAME:
						event = reader.nextEvent();
						flower.setMultiplying(event.asCharacters().getData());
						break;
					case VisualParameters.C_NAME:
						VisualParameters visualParameters = new VisualParameters();
						flower.setVisualParameters(visualParameters);
						parseVisualParameters(visualParameters, reader);
						break;
					case GrowingTips.C_NAME:
						GrowingTips growingTips = new GrowingTips();
						flower.setGrowingTips(growingTips);
						parseGrowingTips(growingTips, reader);
						break;
				}
			}
		}
	}

	public List<Flower> parseFlowers() {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
		try {
			XMLEventReader reader = factory.createXMLEventReader(new FileInputStream(xmlFileName), UTF_8_ENCODING);
			if (reader != null) {
				while (reader.hasNext()) {
					XMLEvent event = reader.nextEvent();
					if (isEmptyContent(event))
						continue;
					if (event.isStartElement()) {
						if (Flower.C_NAME.equals(event.asStartElement().getName().getLocalPart())) {
							Flower flower = new Flower();
							parseFlower(flower, reader);
							flowers.add(flower);
						}
					}
				}
				reader.close();
			}
		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}
		return flowers;
	}

	public static void sort(List<Flower> flowers) {
		flowers.sort(Comparator.comparing(o -> o.getVisualParameters().getAveLenFlower()));
	}

}