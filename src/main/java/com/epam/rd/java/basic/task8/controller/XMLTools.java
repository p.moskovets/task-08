package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Additional tools for XML.
 *
 * Used for XML validation and conversion an object to XML file
 *
 * @author  Pavlo Moskovets
 *
 */
public class XMLTools {

    public static final String SCHEMA = "http://www.w3.org/2001/XMLSchema";
    public static final String XML_SUFFIX = ".xml";
    public static final String XSD_SUFFIX = ".xsd";
    public static final String XML_ROOT = "flowers";
    public static final String XMLNS_ATTR = "xmlns";
    public static final String XMLNS_VAL = "http://www.nure.ua";
    public static final String XMLNS_XSI_ATTR = "xmlns:xsi";
    public static final String XMLNS_XSI_VAL = "http://www.w3.org/2001/XMLSchema-instance";
    public static final String SCHEMA_LOCATION_ATTR = "xsi:schemaLocation";
    public static final String SCHEMA_LOCATION_VAL = "http://www.nure.ua input.xsd";

    private static void addGrowingTips(Element flower, Document document, GrowingTips growingTips) {
        Element growingTipsE = document.createElement(GrowingTips.C_NAME);
        //adding temperature
        Element temperature = document.createElement(GrowingTips.F_TEMPERATURE);
        temperature.setAttribute(GrowingTips.A_MEASURE, growingTips.getTemperatureMeasure());
        temperature.appendChild(document.createTextNode(growingTips.getTemperature().toString()));
        growingTipsE.appendChild(temperature);
        //adding lighting
        Element lighting = document.createElement(GrowingTips.F_LIGHTING);
        lighting.setAttribute(GrowingTips.A_LIGHT_REQUIRING, growingTips.isLightRequiringString());
        growingTipsE.appendChild(lighting);
        //adding temperature
        Element watering = document.createElement(GrowingTips.F_WATERING);
        watering.setAttribute(GrowingTips.A_MEASURE, growingTips.getWateringMeasure());
        watering.appendChild(document.createTextNode(growingTips.getWatering().toString()));
        growingTipsE.appendChild(watering);
        //appending
        flower.appendChild(growingTipsE);
    }

    private static void addVisualParameters(Element flower, Document document, VisualParameters parameters) {
        Element visualParameters = document.createElement(VisualParameters.C_NAME);
        //adding stemColour
        Element stemColour = document.createElement(VisualParameters.F_STEM_COLOUR);
        stemColour.appendChild(document.createTextNode(parameters.getStemColour()));
        visualParameters.appendChild(stemColour);
        //adding leafColour
        Element leafColour = document.createElement(VisualParameters.F_LEAF_COLOUR);
        leafColour.appendChild(document.createTextNode(parameters.getLeafColour()));
        visualParameters.appendChild(leafColour);
        //adding aveLenFlower
        Element aveLenFlower = document.createElement(VisualParameters.F_AVE_LEN_FLOWER);
        aveLenFlower.setAttribute(VisualParameters.A_MEASURE, parameters.getMeasure());
        aveLenFlower.appendChild(document.createTextNode(parameters.getAveLenFlower().toString()));
        visualParameters.appendChild(aveLenFlower);
        //appending
        flower.appendChild(visualParameters);
    }

    public static void writeFlowersXML(List<Flower> flowers, String outputFileName) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            Element root = document.createElement(XML_ROOT);
            root.setAttribute(XMLNS_ATTR, XMLNS_VAL);
            root.setAttribute(XMLNS_XSI_ATTR, XMLNS_XSI_VAL);
            root.setAttribute(SCHEMA_LOCATION_ATTR, SCHEMA_LOCATION_VAL);
            document.appendChild(root);
            for (Flower flower: flowers) {
                //adding flower
                Element flow = document.createElement(Flower.C_NAME);
                root.appendChild(flow);
                //adding flower.name
                Element name = document.createElement(Flower.F_NAME);
                name.appendChild(document.createTextNode(flower.getName()));
                flow.appendChild(name);
                //adding flower.soil
                Element soil = document.createElement(Soil.C_NAME);
                soil.appendChild(document.createTextNode(flower.getSoil().getValue()));
                flow.appendChild(soil);
                //adding flower.origin
                Element origin = document.createElement(Flower.F_ORIGIN);
                origin.appendChild(document.createTextNode(flower.getOrigin()));
                flow.appendChild(origin);
                //adding flower.visualParameters
                addVisualParameters(flow, document, flower.getVisualParameters());
                //adding flower.growingTips
                addGrowingTips(flow, document, flower.getGrowingTips());
                //adding flower.multiplying
                Element multiplying = document.createElement(Multiplying.C_NAME);
                multiplying.appendChild(document.createTextNode(flower.getMultiplying().getValue()));
                flow.appendChild(multiplying);

            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new FileWriter(outputFileName, StandardCharsets.UTF_8));
            transformer.transform(domSource, streamResult);
        } catch (ParserConfigurationException | TransformerException | IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean validateFile(String xmlFile)
    {
        SchemaFactory factory = SchemaFactory.newInstance(SCHEMA);

        Schema schema = null;
        try {
            schema = factory.newSchema(new File(xmlFile.replace(XML_SUFFIX, XSD_SUFFIX)));
        } catch (SAXException e) {
            e.printStackTrace();
        }

        Validator validator = schema.newValidator();
        Source source = new StreamSource(xmlFile);
        try
        {
            validator.validate(source);
            return true;
        }
        catch (SAXException | IOException ex)
        {
            ex.printStackTrace();
            return false;
        }
    }
}
